FROM node:7.7.3-alpine

RUN mkdir -p /app

WORKDIR /app

COPY . /app

RUN yarn install

CMD [ "yarn", "start" ]

ENV PORT 4321

EXPOSE 4321