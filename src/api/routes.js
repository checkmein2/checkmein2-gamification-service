const express = require('express');
const router = express.Router();
const schemaValidator = require('../api-schemas/schema-validator');
const badgeServiceController = require('../repository/badge-service.js');
const pointsServiceController = require('../repository/points-service.js');

router.get('/badges/', (req, res) => {
    badgeServiceController.getAllBadges(req, res);
});
router.get('/badges/:id', (req, res) => {
    badgeServiceController.getBadgesById(req, res);
});

router.get('/badges/user/:id', (req, res) => {
    badgeServiceController.getBadgesById(req, res);
});

router.get('/points/', (req, res) => {
    pointsServiceController.getAllPoints(req, res);
});
router.get('/points/:id', (req, res) => {
    pointsServiceController.getPointsById(req, res);
});

router.put('/badges/:id', (req, res) => {
    badgeServiceController.updateBadge(req, res);
});
router.put('/points/:id', (req, res) => {
    pointsServiceController.updatePoints(req, res);
});

router.post('/badges/:id', schemaValidator('add-badge'), (req, res) => {
    badgeServiceController.addBadge(req, res);
});
router.post('/points/:id', (req, res) => {
    pointsServiceController.addPoints(req, res);
});

router.post('/badges/user/:id', (req, res) => {
    badgeServiceController.awardBadgeToUser(req, res);
});

router.delete('/badges/:id', (req, res) => {
    badgeServiceController.removeBadge(req, res);
});
router.delete('/points/:id', (req, res) => {
    pointsServiceController.removePoints(req, res);
});

module.exports = router;
