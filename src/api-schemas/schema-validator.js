const Ajv = require('ajv');
const ajv = Ajv({ allErrors: true, removeAdditional: 'all' });
const badgesSchema = require('./add-badge');
const pointsSchema = require('./add-points');

ajv.addSchema(badgesSchema, 'add-badge');
ajv.addSchema(pointsSchema, 'add-points');

const errorResponse = schemaErrors => {
    const errors = schemaErrors.map(error => ({
        path: error.dataPath,
        message: error.message
    }));
    return {
        status: 'failed',
        errors
    };
};
const validateSchema = schemaName => (req, res, next) => {
    const valid = ajv.validate(schemaName, req.body);
    if (!valid) {
        return res.send(errorResponse(ajv.errors));
    }
    next();
};

module.exports = validateSchema;

