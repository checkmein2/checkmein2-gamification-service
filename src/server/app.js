// libraries imports
const express = require('express');
const logger = require('../utils/logger.js');
const health = require('../api/health.js');

// app imports
const router = require('../api/routes');

const app = express();
health(app);
const port = process.env.PORT || 3000;

app.use('/', router);

const startGamificationService = () => app.listen(port, (error) => {
    if (error) {
        logger.debug('Gamification service cannot be started');
    }
    logger.log('info', `Gamification service listening on port ${port}`);
});

module.exports = { startGamificationService };
