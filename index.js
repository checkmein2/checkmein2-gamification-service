const app = require('./src/server/app.js');
const fs = require('fs');

if (!fs.existsSync('logs')) {
    fs.mkdirSync('logs');
}
app.startGamificationService();
